package test.ee.bcs.koolitus.toopuhkeajahaldamine.service;

import java.math.BigInteger;

import org.junit.Assert;
import org.junit.Test;

import ee.bcs.koolitus.toopuhkeajahaldamine.service.VacationsAvailable;

public class TestVacationsAvailable
{

	@Test
	public void verifyVacationsAvailableStartingWorkingCurrentYear()
	{
		VacationsAvailable validation = new VacationsAvailable();

		int daysLeft = validation.vacationAvailable(BigInteger.valueOf(Long.valueOf("48811100421")));
		Assert.assertEquals(28, daysLeft);
	}

	@Test
	public void verifyVacationsAvailableStartingLastYear()
	{
		VacationsAvailable validation = new VacationsAvailable();

		int daysLeft = validation.vacationAvailable(BigInteger.valueOf(Long.valueOf("49312010736")));
		Assert.assertEquals(2 * 28, daysLeft);
	}

	@Test
	public void verifyVacationsAvailableStartingElevenYearsAgo()
	{
		VacationsAvailable validation = new VacationsAvailable();

		int daysLeft = validation.vacationAvailable(BigInteger.valueOf(Long.valueOf("38004300598")));
		Assert.assertEquals(12 * 28 - 87, daysLeft);
	}

	@Test
	public void verifyVacationsAvailableStartingTwentyYearsAgo()
	{
		VacationsAvailable validation = new VacationsAvailable();

		int daysLeft = validation.vacationAvailable(BigInteger.valueOf(Long.valueOf("39510310452")));
		Assert.assertEquals(21 * 28 - 77, daysLeft);
	}

}

package test.ee.bcs.koolitus.toopuhkeajahaldamine.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(
{ TestValidationOfVacation.class, TestVacationsAvailable.class })

public class AllTests
{

}

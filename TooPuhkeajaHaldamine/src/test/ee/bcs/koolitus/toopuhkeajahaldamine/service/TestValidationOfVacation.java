package test.ee.bcs.koolitus.toopuhkeajahaldamine.service;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import ee.bcs.koolitus.toopuhkeajahaldamine.service.ValidationOfVacations;

public class TestValidationOfVacation
{

	@Test
	public void verifyValidationOfStartingDateToday()
	{
		ValidationOfVacations validation = new ValidationOfVacations();
		Date testDateOk = new Date();
		Assert.assertEquals(true, validation.isStartingDateValid(testDateOk));
	}

	@Test
	public void verifyValidationOfStartingDateYesterday()
	{
		ValidationOfVacations validation = new ValidationOfVacations();
		Date dateYesterday = new Date(System.currentTimeMillis() - 1000L * 60L * 60L * 24L);
		Assert.assertEquals(false, validation.isStartingDateValid(dateYesterday));
	}

	@Test
	public void verifyValidationOfStartingDateTomorrow()
	{
		ValidationOfVacations validation = new ValidationOfVacations();
		Date dateTomorrow = new Date(System.currentTimeMillis() + 1000L * 60L * 60L * 24L);
		Assert.assertEquals(true, validation.isStartingDateValid(dateTomorrow));
	}

	@Test
	public void verifyValidationOfEndingDateTodayWhenStartingDateToday()
	{
		ValidationOfVacations validation = new ValidationOfVacations();
		Date startDate = new Date();
		Date endDate = new Date();
		Assert.assertEquals(true, validation.isEndingDateValid(startDate, endDate));
	}

	@Test
	public void verifyValidationOfEndingDateBeforStartDate()
	{
		ValidationOfVacations validation = new ValidationOfVacations();
		Date endingDate = new Date(System.currentTimeMillis() - 1000L * 60L * 60L * 24L);
		Date startingDate = new Date();
		Assert.assertEquals(false, validation.isEndingDateValid(startingDate, endingDate));
	}

	@Test
	public void verifyValidationOfEndingDateAfterStartingDate()
	{
		ValidationOfVacations validation = new ValidationOfVacations();
		Date endingDate = new Date(System.currentTimeMillis() + 1000L * 60L * 60L * 24L);
		Date startingDate = new Date();
		Assert.assertEquals(true, validation.isEndingDateValid(startingDate, endingDate));
	}

	/** tests: +7/+6/+8 days */
	@Test
	public void verifyValidationOfStartingDateSevenDaysAfterCurrentDate()
	{
		ValidationOfVacations validation = new ValidationOfVacations();
		Date startDate = new Date(System.currentTimeMillis() + 7 * (1000L * 60L * 60L * 24L));
		Date currentDate = new Date();
		Assert.assertEquals(true, validation.isStartingDateSevenDaysAfterCurrentDate(startDate, currentDate));
	}

	@Test
	public void verifyValidationOfStartingDateSevenDaysAfterCurrentDateDayBefore()
	{
		ValidationOfVacations validation = new ValidationOfVacations();
		Date startDate = new Date(System.currentTimeMillis() + 6 * (1000L * 60L * 60L * 24L));
		Date currentDate = new Date();
		Assert.assertEquals(true, validation.isStartingDateSevenDaysAfterCurrentDate(startDate, currentDate));
	}

	@Test
	public void verifyValidationOfStartingDateSevenDaysAfterCurrentDateDayAfter()
	{
		ValidationOfVacations validation = new ValidationOfVacations();
		Date startDate = new Date(System.currentTimeMillis() + 8 * (1000L * 60L * 60L * 24L));
		Date currentDate = new Date();
		Assert.assertEquals(true, validation.isStartingDateSevenDaysAfterCurrentDate(startDate, currentDate));
	}

	/** tests: 28 days */
	@Test
	public void verifyValidationOfTwentyEightDays()
	{
		ValidationOfVacations validation = new ValidationOfVacations();
		Date startingDate = new Date();
		Date endingDate = new Date(System.currentTimeMillis() + 28 * (1000L * 60L * 60L * 24L));
		Assert.assertEquals(true, validation.hasTwentyEightDays(startingDate, endingDate));
	}

	@Test
	public void verifyValidationOfTwentyEightDaysBefore()
	{
		ValidationOfVacations validation = new ValidationOfVacations();
		Date startingDate = new Date();
		Date endingDate = new Date(System.currentTimeMillis() + 27 * (1000L * 60L * 60L * 24L));
		Assert.assertEquals(true, validation.hasTwentyEightDays(startingDate, endingDate));
	}

	@Test
	public void verifyValidationOfTwentyEightDaysAfter()
	{
		ValidationOfVacations validation = new ValidationOfVacations();
		Date startingDate = new Date();
		Date endingDate = new Date(System.currentTimeMillis() + 29 * (1000L * 60L * 60L * 24L));
		Assert.assertEquals(false, validation.hasTwentyEightDays(startingDate, endingDate));
	}

}
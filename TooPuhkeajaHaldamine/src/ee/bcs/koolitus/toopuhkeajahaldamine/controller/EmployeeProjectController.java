package ee.bcs.koolitus.toopuhkeajahaldamine.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.toopuhkeajahaldamine.bean.businessleader.EmployeeProject;
import ee.bcs.koolitus.toopuhkeajahaldamine.service.EmployeeProjectService;
import ee.bcs.koolitus.toopuhkeajahaldamine.service.EmployeeProjectService1;

@Path("/projects")
public class EmployeeProjectController
{
	EmployeeProjectService1 employeeProjectService = new EmployeeProjectService1();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<EmployeeProject> getEmployeeProjects()
	{

		List<EmployeeProject> listOfEmployeeProjects = employeeProjectService.getAllEmployeeProjects();
		return listOfEmployeeProjects;
	}

	// @GET
	// @Path("/{idCode}") // mille kaudu peaks tee minema?//
	// @Produces(MediaType.APPLICATION_JSON)
	// // public EmployeeProject getEmployeeProjectByIdCode(@PathParam("idCode")
	// String idCode)
	// {
	//
	// }

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public EmployeeProject addEmployeeProject(EmployeeProject employeeproject)
	{
		return EmployeeProjectService.addEmployeeProject(employeeproject);
	}

	// @PUT
	// @Produces(MediaType.APPLICATION_JSON)
	// public EmployeeProject updateEmployeeProject(EmployeeProject
	// employeeproject)
	// {
	// return employeeProjectService.updateEmployeeProject(employeeproject);
	// }
	//
	// @DELETE
	// @Path("/{idCode}")
	// @Produces(MediaType.APPLICATION_JSON)
	// public void deleteEmployeeProject(@PathParam("idCode") String idCode)
	// {
	// employeeProjectService.deleteEmployeeProject(idCode);
	// }
}

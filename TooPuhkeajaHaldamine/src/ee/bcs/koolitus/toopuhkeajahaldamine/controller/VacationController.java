package ee.bcs.koolitus.toopuhkeajahaldamine.controller;

import java.math.BigInteger;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.toopuhkeajahaldamine.bean.Vacation;
import ee.bcs.koolitus.toopuhkeajahaldamine.service.VacationService;
import ee.bcs.koolitus.toopuhkeajahaldamine.service.ValidationOfVacations;

@Path("/vacations")
public class VacationController
{
	VacationService vacationService = new VacationService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Vacation> getVacations()
	{
		List<Vacation> listOfVacations = vacationService.getVacations();
		return listOfVacations;
	}

	@GET
	@Path("/employee/id/{emp_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Vacation> getVacationsByEmployeeName(@PathParam("emp_id") BigInteger employeeid)
	{
		return vacationService.getVacationsByEmployeeId(employeeid);
	}

	/** POST method for adding new vac-s */
	@POST
	@Path("/employee/id/{emp_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Vacation addVacation(@PathParam("emp_id") BigInteger employeeId, Vacation vacation)
	{
		vacation.setEmployeeId(employeeId);

		ValidationOfVacations validation = new ValidationOfVacations();
		if (validation.isStartingDateValid(vacation.getDuration().getStartingDate())
				&& validation.isEndingDateValid(vacation.getDuration().getStartingDate(),
						vacation.getDuration().getEndingDate())
				&& validation.hasTwentyEightDays(vacation.getDuration().getStartingDate(),
						vacation.getDuration().getEndingDate()))
		{
			return vacationService.addVacation(vacation);
		}
		return null;
	}
}

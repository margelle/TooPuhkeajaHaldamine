package ee.bcs.koolitus.toopuhkeajahaldamine.service;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.toopuhkeajahaldamine.bean.DurationOfVacation;
import ee.bcs.koolitus.toopuhkeajahaldamine.bean.Vacation;
import ee.bcs.koolitus.toopuhkeajahaldamine.bean.VacationType;

public class VacationService
{
	public List<Vacation> getVacations()
	{
		List<Vacation> listOfVacations = new ArrayList<>();

		DbConnectionHandling connectionHandler = new DbConnectionHandling();
		Connection connection = connectionHandler.getConnection();

		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM register_project.vacation;");)
		{
			while (resultSet.next())
			{
				DurationOfVacation duration = new DurationOfVacation(resultSet.getDate("starting_date"),
						resultSet.getDate("ending_date"));
				VacationType type = resultSet.getString("vacation_type") == "leave" ? VacationType.LEAVE
						: VacationType.HOLIDAY;
				listOfVacations.add(new Vacation(BigInteger.valueOf(resultSet.getLong("employee_id")), type, duration));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		connectionHandler.closeConnection(connection);
		return listOfVacations;

	}

	public List<Vacation> getVacationsByEmployeeId(BigInteger employeeId)
	{
		List<Vacation> listOfVacations = new ArrayList<>();

		DbConnectionHandling connectionHandler = new DbConnectionHandling();
		Connection connection = connectionHandler.getConnection();

		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"SELECT * FROM register_project.vacation WHERE employee_id = " + employeeId + ";");)
		{
			while (resultSet.next())
			{
				DurationOfVacation duration = new DurationOfVacation(resultSet.getDate("starting_date"),
						resultSet.getDate("ending_date"));
				VacationType type = resultSet.getString("vacation_type").equals("leave") ? VacationType.LEAVE
						: VacationType.HOLIDAY;
				listOfVacations.add(new Vacation(BigInteger.valueOf(resultSet.getLong("employee_id")), type, duration));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		connectionHandler.closeConnection(connection);
		return listOfVacations;
	}

	public List<Vacation> getVacationsByEmployeeId(String employeeId)
	{
		List<Vacation> listOfVacations = new ArrayList<>();

		DbConnectionHandling connectionHandler = new DbConnectionHandling();
		Connection connection = connectionHandler.getConnection();

		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"SELECT * FROM register_project.vacation WHERE employee_id = " + employeeId + ";");)
		{
			while (resultSet.next())
			{
				DurationOfVacation duration = new DurationOfVacation(resultSet.getDate("starting_date"),
						resultSet.getDate("ending_date"));
				VacationType type = resultSet.getString("vacation_type").equals("leave") ? VacationType.LEAVE
						: VacationType.HOLIDAY;
				listOfVacations.add(new Vacation(BigInteger.valueOf(resultSet.getLong("employee_id")), type, duration));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		connectionHandler.closeConnection(connection);
		return listOfVacations;
	}

	/** POST method for adding emp. vacations 13022017 */
	public Vacation addVacation(Vacation vacation)
	{
		DbConnectionHandling connectionHandler = new DbConnectionHandling();
		Connection connection = connectionHandler.getConnection();

		try (Statement statement = connection.createStatement();)
		{
			String query = "INSERT INTO register_project.vacation"
					+ "(employee_id,  vacation_type, starting_date, ending_date) VALUES (" + vacation.getEmployeeId()
					+ ", '" + vacation.getType().toString() + "', '"
					+ new java.sql.Date(vacation.getDuration().getStartingDate().getTime()) + "', '"
					+ new java.sql.Date(vacation.getDuration().getEndingDate().getTime()) + "');";
			statement.executeUpdate(query);

		} catch (SQLException e)
		{

			e.printStackTrace();
		}
		connectionHandler.closeConnection(connection);
		return vacation;
	}

}

package ee.bcs.koolitus.toopuhkeajahaldamine.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ee.bcs.koolitus.toopuhkeajahaldamine.bean.businessleader.EmployeeProject;

public class EmployeeProjectService
{

	static HashMap<String, EmployeeProject> employeeProjectIdMap = getEmployeeProjectIdMap();

	public EmployeeProjectService()
	{
		super();

		if (employeeProjectIdMap == null)
		{
			employeeProjectIdMap = new HashMap<String, EmployeeProject>();

			// EmployeeProject maasikasEmployeeProject = new
			// EmployeeProject("67806190278", "Mari Maasikas1", "Accounting",
			// 2015 - 02 - 01);
			// EmployeeProject vaarikasEmployeeProject = new
			// EmployeeProject("69312010736", "Varje Vaarikas1", "Sales",
			// 2016 - 02 - 07);
			// EmployeeProject puravikEmployeeProject = new
			// EmployeeProject("56807010287", "Paul Puravik1",
			// "Administration", 2013 - 05 - 10);

			// employeeProjectIdMap.put("67806190278", maasikasEmployeeProject);
			// employeeProjectIdMap.put("69312010736", vaarikasEmployeeProject);
			// employeeProjectIdMap.put("56807010287", puravikEmployeeProject);
		}
	}

	public List<EmployeeProject> getAllEmployeeProjects()
	{

		List<EmployeeProject> projects = new ArrayList<EmployeeProject>(employeeProjectIdMap.values());
		return projects;
	}

	public static EmployeeProject getEmployeeProject(String idCode)
	{
		EmployeeProject employeeproject = employeeProjectIdMap.get(idCode);
		return employeeproject;
	}

	public static EmployeeProject addEmployeeProject(EmployeeProject employeeproject)
	{
		// employeeproject.setIdCode(employeeProjectIdMap.size()); // kas mul on
		// vaja üldse
		// otsida
		// idCode
		// alusel?//
		// employeeProjectIdMap.put(employeeproject, getIdCode(),
		// employeeproject);
		return employeeproject;
	}

	private static EmployeeProject getIdCode()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public EmployeeProject updateEmployeeProject(EmployeeProject employeeproject)
	{
		// if (employeeproject.getIdCode() <= 0) // mida tahan sellega öelda?//
		// return null;
		employeeProjectIdMap.put(employeeproject.getIdCode(), employeeproject);
		return employeeproject;
	}

	public void deleteEmployeeProject(String idCode)
	{
		employeeProjectIdMap.remove(idCode);
	}

	public static HashMap<String, EmployeeProject> getEmployeeProjectIdMap()
	{
		return employeeProjectIdMap;
	}
}
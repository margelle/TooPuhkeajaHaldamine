package ee.bcs.koolitus.toopuhkeajahaldamine.service;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.toopuhkeajahaldamine.bean.businessleader.EmployeeProject;

public class EmployeeProjectService1
{
	public List<EmployeeProject> getAllEmployeeProjects()
	{
		List<EmployeeProject> listOfEmployeeProjects = new ArrayList<>();
		DbConnectionHandling connectionHandler = new DbConnectionHandling();
		Connection connection = connectionHandler.getConnection();

		try (Statement statement = connection.createStatement();

				ResultSet resultSet = statement.executeQuery("SELECT * FROM register_project.register;");)
		{
			while (resultSet.next())
			{
				listOfEmployeeProjects
						.add(new EmployeeProject(resultSet.getString("name"), resultSet.getString("idcode"),
								resultSet.getString("departement"), resultSet.getDate("startingdate")));

			}
		} catch (SQLException e)
		{

			e.printStackTrace();
		}
		connectionHandler.closeConnection(connection);
		return listOfEmployeeProjects;
	}

	public EmployeeProject getEmployeeProjectByEmpoyeeId(BigInteger employeeId)
	{
		EmployeeProject employeeProject = null;
		DbConnectionHandling connectionHandler = new DbConnectionHandling();
		Connection connection = connectionHandler.getConnection();

		try (Statement statement = connection.createStatement();

				ResultSet resultSet = statement
						.executeQuery("SELECT * FROM register_project.register WHERE idcode=" + employeeId + ";");)
		{
			while (resultSet.next())
			{

				employeeProject = new EmployeeProject(resultSet.getString("name"), resultSet.getString("idcode"),
						resultSet.getString("departement"), resultSet.getDate("startingdate"));

			}
		} catch (SQLException e)
		{

			e.printStackTrace();
		}
		connectionHandler.closeConnection(connection);
		return employeeProject;
	}
}
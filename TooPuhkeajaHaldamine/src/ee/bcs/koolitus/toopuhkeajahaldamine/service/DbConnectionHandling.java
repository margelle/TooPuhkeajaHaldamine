package ee.bcs.koolitus.toopuhkeajahaldamine.service;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DbConnectionHandling
{

	private String serverName = "localhost";
	private Object dbms = "mysql";
	private String userName = "marge";
	private String password = "Pl114ts";
	private String portNumber = "3306";
	private String dbName = "register_project";

	public Connection getConnection()
	{
		loadDriver();

		Connection conn = null;
		Properties connectionProps = new Properties();
		connectionProps.put("user", this.userName);
		connectionProps.put("password", this.password);
		try
		{
			conn = DriverManager.getConnection(
					"jdbc:" + this.dbms + "://" + this.serverName + ":" + this.portNumber + "/", connectionProps);
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		System.out.println("Connected to database");
		return conn;

	}

	private void loadDriver()
	{
		try
		{
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e)
		{
			System.out.println("Loading MySql driver has failed");
			e.printStackTrace();
		}
	}

	public void register_project(Connection con, String register) throws SQLException
	{

		Statement stmt = null;
		String query = "select * " + "from register_project." + register;

		try
		{
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next())
			{

				String name = rs.getString("name");
				String idCode = rs.getString("idcode");
				String departement = rs.getString("departement");
				Date startingDate = rs.getDate("startingdate");
				System.out.println(name + "\t" + idCode + "\t" + departement + "\t\t" + startingDate + "\t");
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			if (stmt != null)
			{
				stmt.close();
			}

		}
	}

	public void closeConnection(Connection connection)
	{
		try
		{
			connection.close();
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

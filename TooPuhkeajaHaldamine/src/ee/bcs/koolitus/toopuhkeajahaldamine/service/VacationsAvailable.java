package ee.bcs.koolitus.toopuhkeajahaldamine.service;

import java.math.BigInteger;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ee.bcs.koolitus.toopuhkeajahaldamine.bean.Vacation;

public class VacationsAvailable
{

	public int vacationAvailable(BigInteger employeeId)
	{
		long usedVacationsTotal = 0;
		VacationService vacationService = new VacationService();
		List<Vacation> usedVacations = vacationService.getVacationsByEmployeeId(employeeId);

		if (usedVacations.size() > 0)
		{
			for (Vacation vac : usedVacations)
			{
				System.out.println(vac);
				usedVacationsTotal = usedVacationsTotal + usedVacationDays(vac);
			}
		} else
		{
			usedVacationsTotal = 0;
		}

		System.out.println(usedVacationsTotal);

		EmployeeProjectService1 employeeProjectService1 = new EmployeeProjectService1();

		Date startingDate = employeeProjectService1.getEmployeeProjectByEmpoyeeId(employeeId).getStartingDate();
		Calendar cal = Calendar.getInstance();
		cal.setTime(startingDate);
		int startingYear = cal.get(Calendar.YEAR);

		int currentYear = Year.now().getValue();

		int workedYears = (currentYear - startingYear) + 1;

		int maximumVacationDays = workedYears > 0 ? workedYears * 28 : 28;

		long residualVacationDays = (maximumVacationDays - usedVacationsTotal);

		System.out.println(startingYear);
		System.out.println(currentYear);
		System.out.println(workedYears);
		System.out.println(maximumVacationDays);
		System.out.println(residualVacationDays);

		return Math.toIntExact(residualVacationDays);
	}

	public long usedVacationDays(Vacation vacation)
	{
		return ((vacation.getDuration().getEndingDate().getTime() - vacation.getDuration().getStartingDate().getTime())
				/ 86400000) + 1;
	}

}
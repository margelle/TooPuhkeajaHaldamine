package ee.bcs.koolitus.toopuhkeajahaldamine.service;

import java.util.Date;

public class ValidationOfVacations
{
	public boolean isStartingDateValid(Date startDate)
	{
		Date todayDate = new Date();
		if (startDate.compareTo(todayDate) == -1)
		{
			return false;
		}
		return true;
	}

	public boolean isEndingDateValid(Date startDate, Date endDate)
	{
		if (endDate.compareTo(startDate) >= 0)
		{
			return true;
		}
		return false;
	}

	public boolean isStartingDateSevenDaysAfterCurrentDate(Date startDate, Date currentDate)
	{
		if (currentDate.compareTo(startDate) <= 0)
		{
			return true;
		}
		return false;
	}

	/** method for calculating no of days */

	private long daysBetween(Date startingDate, Date endingDate)
	{
		long difference = (endingDate.getTime() - startingDate.getTime()) / 86400000;
		return Math.abs(difference);
	}

	public boolean hasTwentyEightDays(Date startingDate, Date endingDate)
	{
		if (daysBetween(startingDate, endingDate) <= 28)
		{
			return true;
		}
		return false;
	}

}

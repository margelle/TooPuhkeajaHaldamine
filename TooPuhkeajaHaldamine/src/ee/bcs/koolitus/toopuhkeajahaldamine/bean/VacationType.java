package ee.bcs.koolitus.toopuhkeajahaldamine.bean;

public enum VacationType
{
	LEAVE("põhipuhkus"), HOLIDAY("staažipuhkus");

	public String description;

	VacationType(String description)
	{
		this.description = description;
	}

	public String getDescription()
	{
		return this.description;
	}

}

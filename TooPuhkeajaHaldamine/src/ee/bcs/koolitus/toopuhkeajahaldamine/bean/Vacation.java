package ee.bcs.koolitus.toopuhkeajahaldamine.bean;

import java.math.BigInteger;

public class Vacation
{
	VacationType type;
	DurationOfVacation duration;
	BigInteger employeeId;

	public Vacation()
	{

	}

	public Vacation(BigInteger employeeID, VacationType type, DurationOfVacation duration)
	{
		this.employeeId = employeeID;
		this.type = type;
		this.duration = duration;
	}

	public VacationType getType()
	{
		return type;
	}

	public void setType(VacationType type)
	{
		this.type = type;
	}

	public DurationOfVacation getDuration()
	{
		return duration;
	}

	public void setDuration(DurationOfVacation duration)
	{
		this.duration = duration;
	}

	public BigInteger getEmployeeId()
	{
		return employeeId;
	}

	public void setEmployeeId(BigInteger employeeId)
	{
		this.employeeId = employeeId;
	}

	@Override
	public String toString()
	{
		return "Vacation [type=" + type + ", duration=" + duration + ", employeeId=" + employeeId + "]";
	}

}

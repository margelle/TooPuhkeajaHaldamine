package ee.bcs.koolitus.toopuhkeajahaldamine.bean.businessleader;

import java.util.Date;

public class EmployeeProject extends BusinessLeaderProject
{
	private String name;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getIdCode()
	{
		return idCode;
	}

	public void setIdCode(String idCode)
	{
		this.idCode = idCode;
	}

	public String getDepartement()
	{
		return departement;
	}

	public void setDepartement(String departement)
	{
		this.departement = departement;
	}

	public Date getStartingDate()
	{
		return startingDate;
	}

	public void setStartingDate(Date startingDate)
	{
		this.startingDate = startingDate;
	}

	private String idCode;
	private String departement;
	private Date startingDate;

	public EmployeeProject(String name, String idCode, String departement, Date startingDate)
	{
		super();
		this.name = name;
		this.idCode = idCode;
		this.departement = departement;
		this.startingDate = startingDate;
	}
}

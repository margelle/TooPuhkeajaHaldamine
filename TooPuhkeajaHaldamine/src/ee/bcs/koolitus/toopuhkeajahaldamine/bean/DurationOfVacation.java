package ee.bcs.koolitus.toopuhkeajahaldamine.bean;

import java.util.Date;

public class DurationOfVacation
{
	Date startingDate;
	Date endingDate;

	public DurationOfVacation()
	{
		return;
	}

	public DurationOfVacation(Date startingDate, Date endingDate)
	{
		this.startingDate = startingDate;
		this.endingDate = endingDate;
	}

	public Date getStartingDate()
	{
		return startingDate;
	}

	public void setStartingDate(Date startingDate)
	{
		this.startingDate = startingDate;
	}

	public Date getEndingDate()
	{
		return endingDate;
	}

	public void setEndingDate(Date endingDate)
	{
		this.endingDate = endingDate;
	}

	@Override
	public String toString()
	{
		return "DurationOfVacation [startingDate=" + startingDate + ", endingDate=" + endingDate + "]";
	}

}

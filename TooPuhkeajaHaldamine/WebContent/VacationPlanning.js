var currentEmployeeId;

function getVacationForEmployee(employeeId) {
	$
			.getJSON(
					"http://localhost:8080/TooPuhkeajaHaldamine/rest/vacations/employee/id/"
							+ employeeId,
					function(vacations) {
						var tableVacation = "";
						for (var i = 0; i < vacations.length; i++) {
							if (i == 0) {
								currentEmployeeId = vacations[i].employeeId
							}
							tableVacation = tableVacation + "<tr><td>"
									+ vacations[i].type + "</td><td>"
									+ vacations[i].duration.startingDate
									+ "</td><td>"
									+ vacations[i].duration.endingDate
									+ "</td></tr>";
						}
						document.getElementById("vacationTableBody").innerHTML = tableVacation;
					}).fail(function(jqxhr, textStatus, error) {
				var err = textStatus + ", " + error;
				console.log("Request Failed: " + err);
			});
}

function getEmployeeVacations() {
	var employeeId = document.getElementById("employeeId").value;
	console.log(employeeId);
	getVacationForEmployee(employeeId)
}

function addNewVacations() {
	var tableBody = document.getElementById("newVacationTableBody");
	var tableBodyRows = tableBody.children;
	for (var i = 0; i < tableBodyRows.length; i++) {
		addNewVacationFromRow(tableBodyRows[i]);
	}
}

function addNewVacationFromRow(row) {
	//if (row.children[1].children[0].value!="" && row.children[2].children[0].value &&verifyDate(row) && verifyEndingDate(row) && verifyStartingDateSevenDaysAfterCurrentDate(row)) {
		var vacationType = row.children[0].children[0].value == "põhipuhkus" ? "LEAVE"
				: "HOLIDAY";
		var startingDate = row.children[1].children[0].value;
		var endingDate = row.children[2].children[0].value;

		var newVacationJson = {
			"type" : vacationType,
			"duration" : {
				"startingDate" : startingDate.trim(),
				"endingDate" : endingDate.trim()
			},
			"employeeId" : currentEmployeeId
		};
		console.log(newVacationJson);

		var newVacationData = JSON.stringify(newVacationJson);

		$
				.ajax({
					url : "http://localhost:8080/TooPuhkeajaHaldamine/rest/vacations/employee/id/"
							+ currentEmployeeId,
					cache : false,
					type : 'POST',
					data : newVacationData,
					success : function(newVacationData) {
						location.reload();
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						console.log("Status: " + textStatus);
						console.log("Error: " + errorThrown);
					},
					contentType : "application/json; charset=utf-8"
				});
	//}
}

$(function() {
	$("#startingdate1").datepicker();
	$("#startingdate1").datepicker("option", "dateFormat", "yy-mm-dd");
	$("#endingdate1").datepicker();
	$("#endingdate1").datepicker("option", "dateFormat", "yy-mm-dd");
	$("#startingdate2").datepicker();
	$("#startingdate2").datepicker("option", "dateFormat", "yy-mm-dd");
	$("#endingdate2").datepicker();
	$("#endingdate2").datepicker("option", "dateFormat", "yy-mm-dd");
	$("#startingdate3").datepicker();
	$("#startingdate3").datepicker("option", "dateFormat", "yy-mm-dd");
	$("#endingdate3").datepicker();
	$("#endingdate3").datepicker("option", "dateFormat", "yy-mm-dd");
	$("#startingdate4").datepicker();
	$("#startingdate4").datepicker("option", "dateFormat", "yy-mm-dd");
	$("#endingdate4").datepicker();
	$("#endingdate4").datepicker("option", "dateFormat", "yy-mm-dd");
});

function verifyDate(row) {
	// for (var i = 1; i <= 2; i++) {
	// console.log(row);
	var todayDate = new Date();
	var todayDateYear = todayDate.getFullYear();
	var todayDateMonth = todayDate.getMonth() + 1;
	var todayDateDay = todayDate.getDate();
	var startDateArray = row.children[1].children[0].value.split("-");
	var startDateYear = parseInt(startDateArray[0]);
	var startDateMonth = parseInt(startDateArray[1]);
	var startDateDay = parseInt(startDateArray[2]);
	if (todayDateYear > startDateYear || todayDateMonth > startDateMonth
			|| todayDateDay >= startDateDay) {
		alert("Sisestatud kuupäev on minevikus.");
		return false;
	}
	// }
	return true;
}

function verifyEndingDate(row) {
	var startDateArray = row.children[1].children[0].value.split("-");
	var startDateYear = parseInt(startDateArray[0]);
	var startDateMonth = parseInt(startDateArray[1]);
	var startDateDay = parseInt(startDateArray[2]);
	var endDateArray = row.children[2].children[0].value.split("-");
	var endDateYear = parseInt(endDateArray[0]);
	var endDateMonth = parseInt(endDateArray[1]);
	var endDateDay = parseInt(endDateArray[2]);
	if (endDateDay <= startDateDay || endDateMonth < startDateMonth
			|| endDateYear < startDateYear) {
		alert("Lõppkuupäev on varasem kui algkuupäev.");
		return false;
	}
	return true;
}

function verifyStartingDateSevenDaysAfterCurrentDate(row) {
	var startDateArray = row.children[1].children[0].value.split("-");
	var startDateYear = parseInt(startDateArray[0]);
	var startDateMonth = parseInt(startDateArray[1]);
	var startDateDay = parseInt(startDateArray[2]);
	var sevenDaysAfterTodayDate = new Date();
	sevenDaysAfterTodayDate.setDate(sevenDaysAfterTodayDate.getDate() + 7);
	var sevenDaysAfterTodayDateYear = sevenDaysAfterTodayDate.getFullYear();
	var sevenDaysAfterTodayDateMonth = sevenDaysAfterTodayDate.getMonth() + 1;
	var sevenDaysAfterTodayDateDay = sevenDaysAfterTodayDate.getDate();
	if (sevenDaysAfterTodayDateDay > startDateDay || sevenDaysAfterTodayDateMonth > startDateMonth
			|| sevenDaysAfterTodayDateYear > startDateYear) {
		alert("Puhkuse alguskuupäev peab olema 7 päeva hilisem.");
		return false;
	}
	return true;
}